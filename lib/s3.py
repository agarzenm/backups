import boto3
from dotenv import load_dotenv
from pathlib import Path
import os

load_dotenv()

S3_CLIENT = boto3.client('s3', aws_access_key_id=os.getenv('ACCESS_KEY'),aws_secret_access_key=os.getenv('SECRET_KEY'))
S3_BUCKET_NAME = os.getenv('S3_BUCKET')
S3_BUCKET_KEY = 'Backup'

class NotAFileOrDirectory(Exception):
    pass

def __put_file_in_bucket(file_):
    try:
        print('Uploading {}'.format(file_))
        # S3 treats a / as a delimiter for a folder
        S3_CLIENT.upload_file(Filename=str(file_.resolve()), Bucket=S3_BUCKET_NAME, Key=S3_BUCKET_KEY + '/' + file_.name)
        print('Uploaded {}'.format(file_))
    except IsADirectoryError:
        pass

def put_data_in_bucket(directory_or_file):
    # Need to check out walrus operator here to make this cleaner
    if Path(directory_or_file).is_dir():
        directory = Path(directory_or_file)
        for file_ in directory.glob('**/*'):
            __put_file_in_bucket(file_)
    elif Path(directory_or_file).is_file():
        file_ = Path(directory_or_file)
        __put_file_in_bucket(file_)
    else:
        raise NotAFileOrDirectory()

def get_data_from_bucket(download_directory):
    Path(download_directory + S3_BUCKET_KEY).mkdir(parents=True, exist_ok=True)
    s3_response = S3_CLIENT.list_objects(Bucket=S3_BUCKET_NAME)
    s3_response_content = s3_response.get('Contents', None)
    if s3_response_content is None:
        # Needs to be replaced with logging
        print('You have not backed up any files to bucket {}'.format(S3_BUCKET_NAME))
        exit()
    files = [file_.get('Key', None) for file_ in s3_response_content]
    for file_ in files:
        try:
            # We want to be filesystem agnostic so lets not encode the forward slash in the filename or key
            filename = str(Path(download_directory, file_))
            print('Downloading {}'.format(file_))
            S3_CLIENT.download_file(Bucket=S3_BUCKET_NAME, Key=file_, Filename=filename)
            print('Downloaded {}'.format(filename))
        except IsADirectoryError:
            pass

