import argparse

from lib import s3

def main():
    arguments = argparse.ArgumentParser()
    arguments.add_argument('-b', '--backup', nargs='+', help='A list of files and or directories to backup. Recursive by default.')
    arguments.add_argument('-d', '--download', help='A directory to download all your backed up files to. Will attempt to create directory')
    argument_list = arguments.parse_args()
    if argument_list.backup:
        for arg in argument_list.backup:
            s3.put_data_in_bucket(arg)
    if argument_list.download:
        s3.get_data_from_bucket(argument_list.download)

if __name__ == '__main__':
    main()
