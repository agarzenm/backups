# This is currently unstable and has no test coverage. Use at your own risk of data loss.

### Supported OS

Unix-like OS

Windows

### Assumptions made

S3 bucket has SSE encryption via KMS.

Recursively search directories by default.

Absolute file paths used to upload to S3.

### Implications

>Recursively search directories by default.

If you run this against a very deeply nested structure the worst case scenario is that you will get a Recursion limit exception.

>Absolute file paths used to upload to S3.

If there is information in the name of a parent path you would not like to disclose then do not use this tool.

.e.g. /home/very_specific_user_name/projects/highly_identifyable_project_name/*stuff_you_want_to_backup*

A bucket will be created with the full path name.

Again adjust this to your threat model. With SSE via KMS I don't think this is actually an issue as your bucket should not be public by default.

### Setup

This project uses a .env file with placeholders that are empty so if you don't populate them the project will explode.

Set the environment variables found in the .env file in the root of the project.

.e.g. export ACCESS_KEY=your_aws_access_key

Setup a virtual env

```
git clone https://gitlab.com/agarzenm/backups
cd backups
mkdir runtime
python3 -m venv runtime/
source runtime/bin/activate
pip install -r requirements.txt
```

### Usage

```
python backups.py -b /tmp/
```
